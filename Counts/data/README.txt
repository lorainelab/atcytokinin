AtCytoRNASeq/Counts/data/README.txt

This counts data, in the file cyto_TH2.0.5_counts.tsv.gz, was generated using 
the program:  tophat-2.0.5.
on the operated system:  Linux_x86_64
using parameters:
-p 8 
-I 2000 
--min-intron-length=30 #this did not seem to actually be implemented
--solexa-quals 
-o TH2.0.5/BA3 
/lustre/home/aloraine/bin/bowtie2-2.0.0-beta7/indexes/A_thaliana_Jun_2009 fastq/Sep7/BA3.fastq

This is documented in samheader.txt

tophat-2.0.5.Linux_x86_64/tophat -p 8 -I 2000 --min-intron-length=30 --solexa-quals -o TH2.0.5/BA3 /lustre/home/aloraine/bin/bowtie2-2.0.0-beta7/indexes/A_thaliana_Jun_2009 fastq/Sep7/BA3.fastq
