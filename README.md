# README #

This repository describes (and updates) data analysis methods used in:

* Identification of cytokinin-responsive genes using microarray meta-analysis and RNA-Seq in Arabidopsis http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3641208/

### Goals of this repository ###

* Demonstrate RNA-Seq data analysis methods
* Provide foundation data sets needed for analysis of cytokinin function in plants

### Need help or more info? ###

* Contact Ann Loraine aloraine@uncc.edu or Ivory Clabaugh ieclabau@uncc.edu

Ann made the files and code in this repository, and Ivory reviewed it. 

### How to use this repository ###

If you want to use the code for another analysis of a totally different data set, please do not fork the repository.

If you want to expand upon our published analysis, please fork. If you find errors in our code, please fix the errors and issue a pull request with your fix so that we can update our code.

